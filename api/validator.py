from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

def uploadtime_validator(upload_time):
    """Upload time validator for validating Uploaded data"""
    if upload_time <= timezone.now():
         raise ValidationError(
            _('%(upload_time)s Kindly add future date'),
            params={'upload_time': str(timezone.now())},
        )


def participants_validator(participant):
    participants_list = participant.split(",")
    if len(participants_list) > 10:
        raise ValidationError(
            _('%(participant) should Not be greater than 10'),
            params={'participant': participant},
        )
    else:
        for participant_ in hosts_list:
            if len(participant_) > 100:
                raise ValidationError(
                    _('%(participant_) Name is more than 100 characters'),
                    params={'participant_': participant_},
                )