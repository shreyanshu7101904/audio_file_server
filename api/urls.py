from django.urls import path
from . import views

urlpatterns = [
    path("create/", views.AudioFileCreateView.as_view(), name="create_audio_file"),
    path(
        "<str:file_type>/<int:pk>", 
        views.AudioFileUpdateView.as_view(),
        name="update_audio_file"
        ),
]
