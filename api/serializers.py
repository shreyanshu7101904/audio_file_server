from rest_framework import serializers
from . import models

class SongSerializer(serializers.ModelSerializer):
    """Song Models Serializer"""
    class Meta:
        model = models.Song
        fields = ['name', 'duration', 'upload_at']


class PodcastSerializer(serializers.ModelSerializer):
    """PodCast Models Serializer"""
    class Meta:
        model = models.Podcast
        fields = ['name', 'duration', 'upload_at', 'host', 'participants']


class AudioBookSerializer(serializers.ModelSerializer):
    """AudioBook Models Serializer"""
    class Meta:
        model = models.Audiobook
        fields = ['name', 'duration', 'upload_at', 'author', 'narrator']

