from django.db import models
from . import validator

# Create your models here.
class BaseModel(models.Model):
    """Base Models for all tables"""
    name = models.CharField(max_length=100)
    duration = models.PositiveIntegerField()
    upload_at = models.DateTimeField(validators=[validator.uploadtime_validator])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    class Meta:
        abstract = True

    def __str__(self):
        return self.name

class Song(BaseModel):
    """Songs Model"""

    class Meta:
        db_table = 'songs'


class Podcast(BaseModel):
    """Podcast Model"""
    host = models.CharField(max_length=100)
    participants = models.CharField(max_length=1010, validators=[validator.participants_validator])

    class Meta:
        db_table = 'podcasts'

class Audiobook(BaseModel):
    author = models.CharField(max_length=100)
    narrator = models.CharField(max_length=100)

    class Meta:
        db_table = 'audiobooks'