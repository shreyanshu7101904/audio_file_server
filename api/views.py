from django.shortcuts import render, get_object_or_404
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from corelib import get_audio_file, NoSuchFileType

# Create your views here.

class AudioFileCreateView(APIView):
    """
    View to create AudioFiles
    """
    parser_classes = [JSONParser]

    def post(self, request, format=None):
        if "audioFileType" in request.data:
            try:
                audio_file = get_audio_file(request.data["audioFileType"].lower())
                if "audioFileMetadata" in request.data:
                    file_serializer = audio_file.get_serializer_class()(data=request.data["audioFileMetadata"])
                    if file_serializer.is_valid():
                        file_serializer.save(data=request.data["audioFileMetadata"])
                    else:
                        return Response({"Error":file_serializer.errors}, status=400)
                else:
                    return Response({"Error": "Kindly pass Audio Meta data"}, status=400)

            except NoSuchFileType:
                return Response({"Error":"No Such Audio file type"}, status=400)
        else:
            return Response({"Error":"kindly pass file type"}, status=400)



class AudioFileUpdateView(APIView):
    """
    View to Update/List/Delete AudioFiles
    """
    parser_classes = [JSONParser]

    def get(self, request, **kwargs):
        """Get view to get all or single instances"""
        file_type = kwargs.get("file_type", None)
        pk = kwargs.get("pk", None)
        if file_type:
            try:
                audio_file = get_audio_file(file_type.lower())
            except NoSuchFileType:
                return Response({"Error":"No Such Audio file type"}, status=400)
            if pk:
                model_object = get_object_or_404(audio_file.get_model_object().objects.all(), pk=pk)
                return Response({"Data": audio_file.get_serializer_class()(model_object).data})
            else:
                model_object = audio_file.get_model_object().objects.all()
                return Response({"Data": audio_file.get_serializer_class()(model_object).data})
        else:
            return Response({"Error": "Kindly provide file type"}, status_code=400)
        return Response({"Error":"Unable to fetch Data"}, status_code=404)

    def post(self, request, format=None, **kwargs):
        """Post view for updating data"""
        file_type = kwargs.get("file_type", None)
        pk = kwargs.get("pk", None)
        print(file_type, pk)
        if file_type and pk:
            try:
                audio_file = get_audio_file(file_type.lower())
                if "audioFileMetadata" in request.data:
                    model_instance = get_object_or_404(audio_file.get_model_object().objects.all(), pk=pk)
                    file_serializer = audio_file.get_serializer_class()(model_instance, data=request.data["audioFileMetadata"])
                    if file_serializer.is_valid():
                        file_serializer.save(data=request.data["audioFileMetadata"])
                        return Response({"Success":"Data Updated Successfully"})
                    else:
                        return Response({"Error":file_serializer.errors}, status=400)
                else:
                    return Response({"Error": "Kindly pass Audio Meta data"}, status=400)

            except NoSuchFileType:
                return Response({"Error":"No Such Audio file type"}, status=400)
        else:
            return Response({"Error":"kindly pass file type and file id"}, status=400)

    def delete(self, request, format=None, **kwargs):
        """Delete view to delete model instances"""
        file_type = kwargs.get("file_type", None)
        pk = kwargs.get("pk", None)
        print(file_type, pk)
        if file_type and pk:
            try:
                audio_file = get_audio_file(file_type.lower())
                model_instance = get_object_or_404(audio_file.get_model_object().objects.all(), pk=pk)
                model_instance.delete()
                return Response({"Success":"Data Deleted Successfully"})
            except NoSuchFileType:
                return Response({"Error":"No Such Audio file type"}, status=400)
        else:
            return Response({"Error":"kindly pass file type and file id"}, status=400)

