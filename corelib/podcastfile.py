from .basefile import Basefile
from api.serializers import PodcastSerializer
from api.models import Podcast



class Podcastfile(Basefile):
    """Podcast File type"""
    _name = "podcast"
    
    def get_serializer_class(self):
        """Returns Serializer class for Song"""
        return PodcastSerializer

    def get_model_object(self):
        """Returns models object for Song"""
        return Podcast
