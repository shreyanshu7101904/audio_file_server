from .basefile import Basefile
from api.serializers import SongSerializer
from api.models import Song

class Songfile(Basefile):
    """Song File type"""
    _name = "song"
    
    def get_serializer_class(self):
        """Returns Serializer class for Song"""
        return SongSerializer

    def get_model_object(self):
        """Returns models object for Song"""
        return Song
