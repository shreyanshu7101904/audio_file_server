from abc import (
    ABC,
    abstractmethod,
)

class Basefile(ABC):
    """Base file type for all audio files"""

    
    @abstractmethod
    def get_serializer_class(self):
        """Returns Serializer class based on file type"""

    @abstractmethod
    def get_model_object(self):
        """Returns models object based on file type"""