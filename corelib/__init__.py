from . import (
    songfile,
    podcastfile,
    audiofile,
)

_all_file_types = {
    "song": songfile.Songfile,
    "podcast": podcastfile.Podcastfile,
    "audiobook": audiofile.AudioBookfile
}


class NoSuchFileType(Exception):
    """No Such File type Exists"""


def get_audio_file(file_type):
    if file_type in _all_file_types:
        return _all_file_types[file_type]()
    else:
        raise NoSuchFileType
    
