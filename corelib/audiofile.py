from .basefile import Basefile
from api.serializers import AudioBookSerializer
from api.models import Audiobook


class AudioBookfile(Basefile):
    """AudioBook File type"""
    _name = "audiobook"
    
    def get_serializer_class(self):
        """Returns Serializer class for Song"""
        return AudioBookSerializer

    def get_model_object(self):
        """Returns models object for Song"""
        return Audiobook
